﻿using CC.Core.Context;
using CC.Core.Entity;
using CC.Core.Utility;
using System.Collections.Generic;
using System.Linq;

namespace CC.Core.Repository.Impl
{
    public class CategoryRepository : Repository<Category>
    {
        public CategoryRepository(DBContext context)
            : base(context)
        {
        }

        public IList<Entity.Category> GetCategoriesByCategoryLevel(int categoryLevel)
        {
            try
            {
                using (var context = base.Context)
                {
                    var categories = base.All();

                    IQueryable<Category> pCategories = null;

                    for (int i = 1; i <= categoryLevel; i++)
                    {
                        if (i == 1)
                        {
                            pCategories = categories.Where(c => c.ParentCategoryId == -1).OrderBy(c => c.CategoryId);
                        }
                        else
                        {
                            pCategories = (from PC in pCategories
                                           join C in categories on PC.CategoryId equals C.ParentCategoryId
                                           orderby C.CategoryId
                                           select C);
                        }

                        if (pCategories == null || !pCategories.Any())
                            break;
                    }

                    return pCategories == null || !pCategories.Any() ? null : pCategories.ToList();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public Entity.Category GetCategoryByCategoryId(int categoryId)
        {
            try
            {
                using (var context = base.Context)
                {
                    var category = base.Filter(c => c.CategoryId == categoryId).FirstOrDefault();

                    if (category != null && category.ParentCategoryId != -1 && string.IsNullOrEmpty(category.Keywords))
                    {
                        category.Keywords = base.All().ToList().GetDefaultKeyword(category.ParentCategoryId);
                    }

                    return category;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}