﻿using CC.Core.Context;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace CC.Core.Repository.Impl
{
    public class Repository<TObject> : IRepository<TObject>
        where TObject : class
    {
        protected DBContext Context = null;

        public Repository(DBContext context)
        {
            Context = context;
        }

        protected DbSet<TObject> DbSet
        {
            get
            {
                return Context.Set<TObject>();
            }
        }

        public virtual IQueryable<TObject> All()
        {
            return DbSet.AsQueryable();
        }

        public void Dispose()
        {
            if (Context != null)
                Context.Dispose();
        }

        public virtual IQueryable<TObject> Filter(Expression<Func<TObject, bool>> predicate)
        {
            return DbSet.Where(predicate).AsQueryable<TObject>();
        }
    }
}