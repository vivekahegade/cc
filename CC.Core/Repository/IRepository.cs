﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace CC.Core.Repository
{
    public interface IRepository<T> : IDisposable
       where T : class
    {
        IQueryable<T> All();

        IQueryable<T> Filter(Expression<Func<T, bool>> predicate);
    }
}