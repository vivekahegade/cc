﻿using CC.Core.Entity;
using System.Data.Entity;

namespace CC.Core.Context
{
    public class DBContext : DbContext
    {
        public DBContext()
            : base("CCDBConnectionString")
        {
        }

        public DbSet<Category> Categories { get; set; }
    }
}