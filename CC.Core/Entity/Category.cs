﻿namespace CC.Core.Entity
{
    public class Category
    {
        public int CategoryId { get; set; }

        public string Keywords { get; set; }
        public string Name { get; set; }

        public int ParentCategoryId { get; set; }
    }
}