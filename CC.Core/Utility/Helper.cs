﻿using CC.Core.Entity;
using System.Collections.Generic;
using System.Linq;

namespace CC.Core.Utility
{
    public static class Helper
    {
        public static string GetDefaultKeyword(this IList<Category> categories, int parentCategoryId)
        {
            return categories
                           .Where(c => c.CategoryId == parentCategoryId)
                           .Select(c => string.IsNullOrEmpty(c.Keywords) ? GetDefaultKeyword(categories, c.ParentCategoryId) : c.Keywords)
                           .FirstOrDefault();
        }
    }
}