﻿using CC.Core.Context;
using Microsoft.Practices.Unity;

namespace CC.Core.Utility
{
    public static class UnityHelper
    {
        private static UnityContainer _container;

        public static UnityContainer Container
        {
            get
            {
                if (_container == null)
                    _container = new UnityContainer();

                return _container;
            }
        }

        public static void LoadDefaultBindings()
        {
            Container.RegisterType<DBContext, DBContext>(new PerThreadLifetimeManager());
        }
    }
}