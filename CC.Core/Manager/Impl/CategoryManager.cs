﻿using CC.Core.Context;
using CC.Core.Repository.Impl;
using CC.Core.Utility;
using Microsoft.Practices.Unity;
using System.Collections.Generic;

namespace CC.Core.Manager.Impl
{
    public class CategoryManager : ICategoryApi
    {
        private CategoryRepository repository;

        public CategoryManager()
        {
            repository = new CategoryRepository(UnityHelper.Container.Resolve<DBContext>());
        }

        IList<Entity.Category> ICategoryApi.GetCategoriesByCategoryLevel(int categoryLevel)
        {
            return repository.GetCategoriesByCategoryLevel(categoryLevel);
        }

        Entity.Category ICategoryApi.GetCategoryByCategoryId(int categoryId)
        {
            return repository.GetCategoryByCategoryId(categoryId);
        }
    }
}