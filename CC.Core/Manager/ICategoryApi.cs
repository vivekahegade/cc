﻿using System.Collections.Generic;

namespace CC.Core.Manager
{
    public interface ICategoryApi
    {
        IList<Entity.Category> GetCategoriesByCategoryLevel(int categoryLevel);

        Entity.Category GetCategoryByCategoryId(int categoryId);
    }
}