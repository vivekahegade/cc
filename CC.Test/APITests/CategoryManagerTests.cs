﻿using CC.Core.Manager;
using CC.Core.Manager.Impl;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace CC.Test.APITests
{
    [TestClass]
    public class CategoryManagerTests : BaseManagerTest<CategoryManager>
    {
        private ICategoryApi categoryManager;

        [TestCleanup]
        public override void CleanUp()
        {
            base.CleanUp();
            categoryManager = null;
        }

        [TestInitialize]
        public override void Init()
        {
            base.Init();
            categoryManager = new CategoryManager();
        }

        #region CategoryManager_GetCategoryByCategoryId

        [TestMethod]
        public void GetCategoryByCategoryId_InvalidId_Returns_NULL()
        {
            var category = categoryManager.GetCategoryByCategoryId(10000);

            Assert.IsNull(category);
        }

        [TestMethod]
        public void GetCategoryByCategoryId_ValidId_Returns_KeywordsFromParent_Level1()
        {
            //Input: 201; Output: ParentCategoryID=200, Name=Computer, Keywords=Teaching
            var category = categoryManager.GetCategoryByCategoryId(201);

            Assert.IsNotNull(category);
            Assert.AreEqual(category.ParentCategoryId, 200);
            Assert.AreEqual(category.Name, "Computer");
            Assert.AreEqual(category.Keywords, "Teaching");
        }

        [TestMethod]
        public void GetCategoryByCategoryId_ValidId_Returns_KeywordsFromParent_Level2()
        {
            //Input: 202; Output: ParentCategoryID=201, Name=Operating System,Keywords=Teaching
            var category = categoryManager.GetCategoryByCategoryId(202);

            Assert.IsNotNull(category);
            Assert.AreEqual(category.ParentCategoryId, 201);
            Assert.AreEqual(category.Name, "Operating System");
            Assert.AreEqual(category.Keywords, "Teaching");
        }

        [TestMethod]
        public void GetCategoryByCategoryId_ValidId_Returns_ValidValues()
        {
            //Input: 100; Output: ParentCategoryID=-1, Name=Business, Keywords=Money
            var category = categoryManager.GetCategoryByCategoryId(100);

            Assert.IsNotNull(category);
            Assert.AreEqual(category.ParentCategoryId, -1);
            Assert.AreEqual(category.Name, "Business");
            Assert.AreEqual(category.Keywords, "Money");
        }

        #endregion CategoryManager_GetCategoryByCategoryId

        #region CategoryManager_GetCategoriesByCategoryLevel

        [TestMethod]
        public void GetCategoriesByCategoryLevel_Returns_NULL_InvalidLevel()
        {
            var categories = categoryManager.GetCategoriesByCategoryLevel(100);

            Assert.IsNull(categories);
        }

        [TestMethod]
        public void GetCategoriesByCategoryLevel_Returns_ValidValues_Level1()
        {
            var categories = categoryManager.GetCategoriesByCategoryLevel(1);

            Assert.IsNotNull(categories);
            Assert.IsTrue(categories.Count == 2);
            Assert.IsTrue(categories.Select(c => c.CategoryId).SequenceEqual(new int[2] { 100, 200 }));
        }

        [TestMethod]
        public void GetCategoriesByCategoryLevel_Returns_ValidValues_Level2()
        {
            //Input: 2; Output: 101, 102, 201
            var categories = categoryManager.GetCategoriesByCategoryLevel(2);

            Assert.IsNotNull(categories);
            Assert.IsTrue(categories.Count == 3);
            Assert.IsTrue(categories.Select(c => c.CategoryId).SequenceEqual(new int[3] { 101, 102, 201 }));
        }

        [TestMethod]
        public void GetCategoriesByCategoryLevel_Returns_ValidValues_Level3()
        {
            //Input: 3; Output: 103, 109, 202
            var categories = categoryManager.GetCategoriesByCategoryLevel(3);

            Assert.IsNotNull(categories);
            Assert.IsTrue(categories.Count == 3);
            Assert.IsTrue(categories.Select(c => c.CategoryId).SequenceEqual(new int[3] { 103, 109, 202 }));
        }

        #endregion CategoryManager_GetCategoriesByCategoryLevel
    }
}