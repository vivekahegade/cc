﻿using CC.Core.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CC.Test.APITests
{
    public abstract class BaseManagerTest<T>
    {
        [TestCleanup]
        public virtual void CleanUp()
        {
            //TODO: Clean up DB/Test data
        }

        [TestInitialize]
        public virtual void Init()
        {
            //TODO: inject the proxy DBContext, load test values and test without DB
            UnityHelper.LoadDefaultBindings();
        }
    }
}