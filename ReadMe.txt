
***Coding Challenge 1*********************

ER & Class diagram of the Store Operation Hours is present in Solution2Problem1_StoreDesign.pdf

***Coding Challenge 2*********************

How to read the Code 
1)	Project "CC.Core" includes all the base/core business logics
2)	Project "CC.Test" includes all the core business logic tests
3)	DB  folder includes the backup DB and Script to generate database/tables 


Below has been implemented as part of the challenge
1)	EF Code first has been used for data access
2)	TDD approach is used by making use of MS Unit tests
3)	The Microsoft Unity framework has been used for dependency injection


Below things could have been implemented and could not able to complete because of the time constraint  
1)	UI/Console app to test the APP
2)	EF migration for DB schema generation
3)	EF context mocking or using dependency resolvers for unit tests
4)	Clean implementation of Unit tests with dependent data creation and disposal
5)  More unit tests for testing the complex scenarios

Note: To run the application/test the scenarios, follow as below,
1)	Run DB Scripts or restore DB
2)	Change the DB Connection string if necessary in app.config
3)	Build the project (on build all the nuget packages will get installed)
4)	Run the unit tests
